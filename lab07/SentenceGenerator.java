import java.util.Random;
import java.util.Scanner;

public class SentenceGenerator{
  public static String randomizeAdjective(String adjectiveWord){
    Random rand = new Random();
    adjectiveWord = adjective(rand.nextInt(10));
    return adjectiveWord;
  }
  public static String randomizeVerb(String verbWord){
    Random rand = new Random();
    verbWord = verb(rand.nextInt(10));
    return verbWord;
  }
  public static String randomizeNouns(String nounword){
    Random rand = new Random();
    int random = rand.nextInt(2);
    if(random == 1){
      nounword = "It";
      return nounword;
    }
    else return nounword;
  }
  public static void paragraph(String nounWord){
    String subject = nounWord;
    Random rand = new Random();
    for(int i = 0; i <= rand.nextInt(20); i++){
      String adjectiveWord = adjective(rand.nextInt(10));
      String adjectiveWord2 = adjective(rand.nextInt(10));
      String verbWord = verb(rand.nextInt(10));
      String nounWord2 = objectNouns(rand.nextInt(10));
      int random = rand.nextInt(6);
      switch(random){
        case 0: System.out.println("This " + subject + " was " + adjectiveWord + " and " + verbWord +" the " + adjectiveWord2 + " " + nounWord2); break;
        case 1: System.out.println("That " + nounWord + " " + verbWord + " it's " + nounWord2);break;
        case 2: System.out.println(randomizeNouns(subject) + " " + randomizeVerb(verbWord) + " the " + randomizeAdjective(adjectiveWord) + " " + nounWord2);break;
        case 3: System.out.println("Will the " + subject + " ever " + randomizeVerb(verbWord) + " the " + randomizeAdjective(adjectiveWord2) + " " + nounWord2 + "?");break;
        case 4: System.out.println(randomizeNouns(subject) + " was very " + randomizeAdjective(adjectiveWord) + " after that."); break;
        case 5: System.out.println("When will " + randomizeNouns(subject)+ " ever " + randomizeVerb(verbWord));
      }
    }
  }
  public static void main(String [] args){
    Random rand = new Random();
    Scanner scan = new Scanner(System.in);
    String adjectiveWord = adjective(rand.nextInt(10));
    String adjectiveWord2 = adjective(rand.nextInt(10));
    int subject = rand.nextInt(10);
    String nounWord = subjectNouns(subject);
    String verbWord = verb(rand.nextInt(10));
    String nounWord2 = objectNouns(rand.nextInt(10));
    System.out.println("The " + adjectiveWord + " " + nounWord +" " + verbWord + " the " + adjectiveWord2 + " " + nounWord2 + ".");
    System.out.println("Would you like more sentences? 1 yes 0 no");
    int choice = scan.nextInt();
    int random = rand.nextInt(1);
    String objectIT = "it";
    
    if( choice == 1){
       adjectiveWord = adjective(rand.nextInt(10));
       adjectiveWord2 = adjective(rand.nextInt(10));
       verbWord = verb(rand.nextInt(10));
       nounWord2 = objectNouns(rand.nextInt(10));
      System.out.println("The " + nounWord +" " + verbWord + " the " + adjectiveWord2 + " " + nounWord2 + ".");
      if(random == 1){
      objectIT = nounWord;
    }
      adjectiveWord = adjective(rand.nextInt(10));
       adjectiveWord2 = adjective(rand.nextInt(10));
       verbWord = verb(rand.nextInt(10));
       nounWord2 = objectNouns(rand.nextInt(10));
      System.out.println(nounWord + " " + verbWord + " " + "the " + adjectiveWord2 + " " + nounWord2);
    }
    paragraph(nounWord);
  }   
    public static String adjective(int random){
      String string = " ";
      switch(random){
        case 0: string = "pretty";
          break;
        case 1: string = "hot";
          break;
        case 2: string =("ugly");
          break;
        case 3: string =("long");
          break;
        case 4: string =("fanatical");
          break;
        case 5: string =("dry");
          break;
        case 6: string =("moist");
          break;
        case 7: string =("rock-solid");
          break;
        case 8: string =("drafty");
          break;
        case 9: string =("wet");
          break;
      }
      return string;
    }
    public static String subjectNouns(int random){
      String string = " ";
      switch(random){
        case 0: string =("rock");
          break;
        case 1: string =("table");
          break;
        case 2: string =("jacket");
          break;
        case 3: string =("desk");
          break;
        case 4: string =("alex");
          break;
        case 5: string =("teacher");
          break;
        case 6: string =("hand");
          break;
        case 7: string =("job");
          break;
        case 8: string =("Rabit");
          break;
        case 9: string =("Wolf");
          break;
      }
      return string;
    }
     public static String verb(int random){
       String string = " ";
      switch(random){
        case 0: string =("smacked");
          break;
        case 1: string =("threw");
          break;
        case 2: string =("follwed");
          break;
        case 3: string =("attacked");
          break;
        case 4: string =("closed");
          break;
        case 5: string =("drank");
          break;
        case 6: string =("finagled");
          break;
        case 7: string =("read");
          break;
        case 8: string =("blew");
          break;
        case 9: string =("kicked");
          break;
      }
       return string;
    }
  public static String objectNouns(int random){
    String string = " ";
      switch(random){
        case 0: string =("tree");
          break;
        case 1: string =("dirt");
          break;
        case 2: string =("animal");
          break;
        case 3: string =("bread");
          break;
        case 4: string =("cactus");
          break;
        case 5: string =("phone");
          break;
        case 6: string =("hippo");
          break;
        case 7: string =("guy");
          break;
        case 8: string =("plant");
          break;
        case 9: string =("computer");
          break;
      }
    return string;
    }
}