import java.util.Random; //import Random Scanner
import java.util.Scanner; //import User Scanner
public class Yahtzee{
  public static void main(String[] args){
    Random rollDice = new Random();
    Scanner myScanner = new Scanner( System.in );
    System.out.print("Enter values of Dice or random? Type 1 for yes or 0 for no\n");
    int yn = myScanner.nextInt(); //declare all variables
    int total = 0;
    int chance = 0;
    int ones = 0;
    int twos = 0;
    int threes = 0;
    int fours = 0;
    int fives = 0;
    int sixes = 0;
    int roll1 = 0;
    int roll2 = 0;
    int roll3 = 0;
    int roll4 = 0;
    int roll5 = 0;
    int threeOfaKind = 0;
    int fourOfaKind = 0;
    int yahtzee = 50;
    int fullHouse = 25;
    int smallStraight = 30;
    int largeStraight = 40;
    if (yn == 1){ //if yes then then enter 5 digit number
      System.out.println("Input a 5 digit number where each number inside is greater than 0 but smaller than 7");
      int roll = myScanner.nextInt();
      if (roll > 66666){ //exit because it is not 5 digits
        System.out.println("Error you did not enter a 5 digit number with the correct parameters. ");
        return;
      }
      else{
      while (roll > 0){ //assign number to variables and check if number is between 1 and 6
        if (roll <= 6){
          roll1 = roll;
          roll = roll/10;
          if (roll1 == 0 || roll1 >= 7){
            System.out.println("Your number inside is greater than 7 or is 0");
            return;
            }
            }
        else if (roll <= 66){
          roll2 = roll%10;
          roll = roll/10;
          if (roll2 == 0 || roll2 >= 7){
            System.out.println("Your number inside is greater than 7 or is 0");
            return;
          }
          }
          else if (roll <= 666){
          roll3 = roll%10;
          roll = roll/10;
          if (roll3 == 0 || roll3 >= 7){
            System.out.println("Your number inside is greater than 7 or is 0");
            return;
          }
          }
          else if (roll <= 6666){
          roll4 = roll%10;
          roll = roll/10;
          if (roll4 == 0 || roll4 >= 7){
            System.out.println("Your number inside is greater than 7 or is 0");
            return;
          }
          }
          else if (roll <= 66666){
          roll5 = roll%10;
          roll = roll/10;
          if (roll5 ==0 || roll5 >= 7){
            System.out.println("Your number inside is greater than 7 or is 0");
            return;
          }
          }
      }
      }
    }
    else if (yn == 0){ //if no then assign random numbers from 1 to 6
      roll1 = rollDice.nextInt(6) + 1;
      roll2 = rollDice.nextInt(6) + 1;
      roll3 = rollDice.nextInt(6) + 1;
      roll4 = rollDice.nextInt(6) + 1;
      roll5 = rollDice.nextInt(6) + 1;
    }
    else { //else error
      System.out.println("You did not type 1 or 0");
      return; 
    }
    System.out.println("Numbers: " + roll1 + " " + roll2 + " " + roll3+ " " + roll4 + " " + roll5);
    //print rolls
    for (int turn = 1; turn <= 6; turn++){ //assign aces, twos... form rolls
      int count = 0;
      if (roll1 == turn){
        count++;
      }
      if (roll2 == turn){
        count++;
      }
      if (roll3 == turn){
        count++;
      }
      if (roll4 == turn){
        count++;
      }
      if (roll5 == turn){
        count++;
      }
      if (turn == 1){
        ones = count;
      }
      if (turn == 2){
        twos = count * 2;
      }
      if(turn == 3){
        threes = count * 3;
      }
      if (turn == 4){
        fours = count * 4;
      }
      if (turn == 5){
        fives = count * 5;
      }
      if (turn == 6){
        sixes = count * 6;
      }
    }
    System.out.println(ones + " " + twos + " " + threes + " " + fours + " " + fives + " " + sixes); //print aces, twos ...
    total = ones + twos + threes + fours + fives + sixes; //create total
    chance = total; //create chance
    if (total > 63){
      total = total + 35;
    }
    System.out.println("total of top: "+ total); //print total of top
    
    if (roll1 == roll2 && roll2 == roll3){ //find three of kind
      threeOfaKind = roll1 * 3;
    }
    else if (roll1 == roll3 && roll3 == roll4){
      threeOfaKind = roll1 * 3;
    }
    else if (roll1 == roll4 && roll4 == roll5){
      threeOfaKind = roll1 * 3;
    }
    else if (roll2 == roll3 && roll3 == roll4){
      threeOfaKind = roll2 * 3;
    }
    else if (roll2 == roll4 && roll4 == roll5){
      threeOfaKind = roll2 * 3;
    }
    else if (roll2 == roll5 && roll5 == roll1){
      threeOfaKind = roll2 * 3;
    }
    else if (roll3 == roll4 && roll4 == roll5){
      threeOfaKind = roll3 * 3;
    }
    else if (roll3 == roll5 && roll5 == roll1){
      threeOfaKind = roll3 * 3;
    }
    System.out.println("three of a kind: "+ threeOfaKind); //print three of a kind
    total = total + threeOfaKind; //add to total
    if (roll1 == roll2 && roll2 == roll3 && roll3 == roll4){ //find four of kind
      fourOfaKind = roll1 * 4;
    }
    else if (roll1 == roll3 && roll3 == roll4 && roll4 == roll5){
      fourOfaKind = roll1 * 4;
    }
    else if (roll2 == roll3 && roll3 == roll4 && roll4 == roll5){
      fourOfaKind = roll2 * 4;
    }
    else if (roll3 == roll4 && roll4 == roll5 && roll5 == roll1){
      fourOfaKind = roll3 * 4;
    }
    else if (roll4 == roll5 && roll5 == roll1 && roll1 == roll2){
      fourOfaKind = roll2 * 4;
    }
    else if (roll5 == roll1 && roll1 == roll2 && roll2 == roll3){
      fourOfaKind = roll2 * 4;
    }
    total = total + fourOfaKind; //add to total
    System.out.println("four of a kind: " + fourOfaKind); //print four of a kind
    //check for small straight
    if (roll1 == roll2 + 1 && roll2 == roll3 + 1 && roll3 == roll4 + 1){
      total = smallStraight + total;
      System.out.println("small Straight");
    }
    else if (roll1 == roll3 + 1 && roll3 == roll4 + 1 && roll4 == roll5 + 1){
      total = smallStraight + total;
      System.out.println("small Straight");
    }
    else if (roll2 == roll3 + 1 && roll3 == roll4 + 1 && roll4 == roll5 + 1){
     total = smallStraight + total;
      System.out.println("small Straight");
    }
    else if (roll3 == roll4 + 1 && roll4 == roll5 + 1 && roll5 == roll1 + 1){
      total = smallStraight + total;
      System.out.println("small Straight");
    }
    else if (roll4 == roll5 + 1 && roll5 == roll1 + 1 && roll1 == roll2 + 1){
      total = smallStraight + total;
      System.out.println("small Straight");
    }
    else if (roll5 == roll1 + 1 && roll1 == roll2 + 1 && roll2 == roll3 + 1){
      total = smallStraight + total;
      System.out.println("small Straight");
    }
    else if (roll1 == roll4 + 1 && roll4 == roll5 + 1 && roll5 == roll3 + 1){
      total = smallStraight + total;
      System.out.println("small Straight");
    
    }
    else if (roll1 == roll2 - 1 && roll2 == roll3 - 1 && roll3 == roll4 - 1){
      total = smallStraight + total;
      System.out.println("small Straight");
    }
    else if (roll1 == roll3 - 1 && roll3 == roll4 - 1 && roll4 == roll5 - 1){
      total = smallStraight + total;
      System.out.println("small Straight");
    }
    else if (roll2 == roll3 - 1 && roll3 == roll4 - 1 && roll4 == roll5 - 1){
     total = smallStraight + total;
      System.out.println("small Straight");
    }
    else if (roll3 == roll4 - 1 && roll4 == roll5 - 1 && roll5 == roll1 - 1){
      total = smallStraight + total;
      System.out.println("small Straight");
    }
    else if (roll4 == roll5 - 1 && roll5 == roll1 - 1 && roll1 == roll2 - 1){
      total = smallStraight + total;
      System.out.println("small Straight");
    }
    else if (roll5 == roll1 - 1 && roll1 == roll2 - 1 && roll2 == roll3 - 1){
      total = smallStraight + total;
      System.out.println("small Straight");
    }
    else if (roll1 == roll4 - 1 && roll4 == roll5 - 1 && roll5 == roll3 - 1){
      total = smallStraight + total;
      System.out.println("small Straight");
    }
    
    //check for large straight
    
    if (roll1 == roll2 + 1 && roll2 == roll3 + 1 && roll3 == roll4 + 1 && roll4 == roll5 + 1){
      total = largeStraight + total;
      System.out.println("large Straight");
    }
    else if (roll1 == roll3 + 1 && roll3 == roll4 + 1 && roll4 == roll5 + 1 && roll5 == roll2 + 1){
      total = largeStraight + total;
      System.out.println("large Straight");
    }
    else if (roll2 == roll3 + 1 && roll3 == roll4 + 1 && roll4 == roll5 + 1 && roll5 == roll1 + 1){
     total = largeStraight + total;
      System.out.println("large Straight");
    }
    else if (roll3 == roll4 + 1 && roll4 == roll5 + 1 && roll5 == roll1 + 1 && roll1 == roll2 + 1){
      total = largeStraight + total;
      System.out.println("large Straight");
    }
    else if (roll4 == roll5 + 1 && roll5 == roll1 + 1 && roll1 == roll2 + 1 && roll2 == roll3 + 1){
      total = largeStraight + total;
      System.out.println("large Straight");
    }
    else if (roll5 == roll1 + 1 && roll1 == roll2 + 1 && roll2 == roll3 + 1 & roll3 == roll4 + 1){
      total = largeStraight + total;
      System.out.println("large Straight");
    }
    else if (roll1 == roll4 + 1 && roll4 == roll5 + 1 && roll5 == roll3 + 1 && roll3 == roll2 + 1){
      total = largeStraight + total;
      System.out.println("large Straight");
    
    }
    
    else if (roll1 == roll2 - 1 && roll2 == roll3 - 1 && roll3 == roll4 - 1 && roll4 == roll5 - 1){
      total = largeStraight + total;
      System.out.println("large Straight");
    }
    else if (roll1 == roll3 - 1 && roll3 == roll4 - 1 && roll4 == roll5 - 1 && roll5 == roll2 - 1){
      total = largeStraight + total;
      System.out.println("large Straight");
    }
    else if (roll2 == roll3 - 1 && roll3 == roll4 - 1 && roll4 == roll5 - 1 && roll5 == roll1 - 1){
     total = largeStraight + total;
      System.out.println("large Straight");
    }
    else if (roll3 == roll4 - 1 && roll4 == roll5 - 1 && roll5 == roll1 - 1 && roll1 == roll2 - 1){
      total = largeStraight + total;
      System.out.println("large Straight");
    }
    else if (roll4 == roll5 - 1 && roll5 == roll1 - 1 && roll1 == roll2 - 1 && roll2 == roll3 - 1){
      total = largeStraight + total;
      System.out.println("large Straight");
    }
    else if (roll5 == roll1 - 1 && roll1 == roll2 - 1 && roll2 == roll3 - 1 & roll3 == roll4 - 1){
      total = largeStraight + total;
      System.out.println("large Straight");
    }
    else if (roll1 == roll4 - 1 && roll4 == roll5 - 1 && roll5 == roll3 - 1 && roll3 == roll2 - 1){
      total = largeStraight + total;
      System.out.println("large Straight");
    
    }
      //check for yahtzee
    if (roll1 == roll2 && roll2 == roll3 && roll3 == roll4 && roll4 == roll5){
      System.out.println("yahtzee");
      total = total + yahtzee;
    }
    else{
      System.out.println("no Yahtzee");
    }
    System.out.println("chance: " + chance); //print chance 
    total = total + chance; // add chance to total
  System.out.println("total: "+ total); //print grand total
  }
}