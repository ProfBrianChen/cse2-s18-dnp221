import java.util.Random; //import random
public class RobotCity{//class robotcity
  public static int[][] update(int[][] city){//update method
    for(int i = 0; i < city.length; i++){
      for(int j = 0; j < city[i].length-1; j++){
       if((city[i][j] < 0) && (city[i][j+1] > 0)){ //check if current area and area after it is zero
         city[i][j+1] = -city[i][j+1];//turn area negative
         j++;
        }
      }
    }
    return city;
  }
  public static int[][] invade(int[][] city, int k){//invade method
    Random rand = new Random();//random
    int row;
    int column;
    for(int i = k; i > 0; i--){
      row = rand.nextInt(city.length);//pick random areas
      column = rand.nextInt(city[i].length);
      if(city[row][column] > 0){
        city[row][column] = - city[row][column];//turn areas negative
      }
      else{i++;}
    }
    return city;
  }
  public static void display(int[][] city){
    for(int i = 0; i < city.length; i++){//display array
      System.out.println();
      for(int j = 0; j < city[i].length; j++){
        System.out.printf("%6d", city[i][j]);//print 
      }
    }
  }
  public static int[][] buildCity(){
    Random rand = new Random();
    int northSouth = rand.nextInt(6) + 10; 
    int eastWest = rand.nextInt(6) + 10;//random dementions
    int[][] city = new int[northSouth][eastWest];
    for(int i = 0; i < northSouth; i++){
      for(int j = 0; j < eastWest; j++){
        city[i][j] = rand.nextInt(900) + 100;//random population values
      }
    }
    return city;
  }
  public static void main(String[] args){
    int[][] city = new int[0][0];//outputs
    city = buildCity();
    display(invade(city, 5));
    System.out.println();
    for(int i = 0; i < 5; i++){
      city = update(city);
      display(city);
      System.out.println();
    }
  }
}