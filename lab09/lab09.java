public class lab09{
  public static int[] copy(int list[]){
    int[] arrayCopy = new int[list.length];
    for(int i = 0; i < list.length; i++){
      arrayCopy[i] = list[i];
    }
    return arrayCopy;
  }
  public static int[] inverter(int list[]){
    int[] arrayCopy = new int[list.length];
    for(int i = 0; i < list.length-1; i++){
      arrayCopy[i] = list[list.length - i -1];
    }
    list = arrayCopy;
    return list;
  }
  public static int[] inverter2(int list[]){
    int[] arrayCopy = new int[list.length];
    arrayCopy = copy(list);
    arrayCopy = inverter(arrayCopy);
    return arrayCopy;
  }
  public static void print(int list[]){
    for(int i = 0; i < list.length; i++){
      System.out.print(list[i] + " ");
    }
  }
  public static void main(String[] args){
    int[] array0 = new int[]{0,1,2,3,4,5,6,7};
    int[] array1 = new int[8];
    int[] array2 = new int[8];
    
    array1 = copy(array0);
    array2 = copy(array0);
    array0 = inverter(array0);
    System.out.print("array0: ");
    print(array0);
    inverter2(array1);
    System.out.print("array1: ");
    print(array1);
    int[] array3 = inverter2(array2);
    System.out.print("array3: ");
    print(array3);
    
    }
  }

