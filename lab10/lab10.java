import java.util.Random; //import random
public class lab10{
  public static int[][] addMatrix( int[][] a, boolean formata, int[][] b, boolean formatb){ //addMatrix Method 
    boolean compatable;
    int[][] addedMatrix = new int[a.length][a[0].length];
    if(formata = false){ //check format
      a = translate(a);
    }
    if(formatb = false){
      b = translate(b);
    }
    if(a.length == b.length && a[0].length == b[0].length){ //check compatability
      compatable = true;
    }
    else{
       System.out.println("The arrays cannot be added");
       compatable = false; 
       return null;
    }
      
    if(compatable = true){ //add matrix 
      for(int i = 0; i < a.length; i++){
        for(int j = 0; j < a[i].length; j++){
          addedMatrix[i][j] = a[i][j] + b[i][j];
        }
      }
    }
    return addedMatrix;
  }
  
  public static int[][] translate(int[][] array){ //translate method
    int[][] translatedArray= new int[array[0].length][array.length];
    for(int i = 0; i < array.length; i ++){
      for (int j = 0; j < array[i].length; j ++){//switch columns and rows
        translatedArray[j][i] = array[i][j];
      }
    }
    return translatedArray;
  }
  public static int[][] increasingMatrix(int width, int height, boolean format){ //increasingMatrix method
    int[][] array = new int[width][height];
    int k = 1;
    if(format == true){//check format
      for(int i =0; i < width; i++){//increse matrix
        for(int j = 0; j< height; j++){
          array[i][j] = k;
          k++;
        }
      }
      return array;
    }
    else if(format == false){//check format
      for(int i = 0; i < height; i++){//increase matrix
        for(int j = 0; j < width; j++){
          array[j][i] = k;
          k++;
        }
      }
    }
    return array;
  }
  public static void printMatrix(int[][] array, boolean format){//method printMatrix
    if(array == null){//check if there is  matrix and spaces
      System.out.println("Array is empty");

    }
    else if(format == true){//check format
      for(int i =0; i < array.length; i++){
        System.out.println();
        for(int j = 0; j<array[i].length; j++){ //print
          System.out.printf(array[i][j] + " ");
        }
      }
    }
    else if(format == false){//check format
      for(int i =0; i < array.length; i++){
        System.out.println();
        for(int j = 0; j<array[i].length; j++){//print
          System.out.printf(array[j][i] + " ");
        }
      }
    }
  }
  public static void main(String[] args){//main
    Random rand = new Random();//create vars 
    int width = rand.nextInt(5) + 1;
    int height = rand.nextInt(5) + 1;
    int width2 = rand.nextInt(5) + 1;
    int height2 = rand.nextInt(5) + 1;
    int[][] A = new int[width][height];
    int[][] B = new int[width][height];
    int[][] C = new int[width2][height2];
    System.out.println("width: " + width);//print width and height
    System.out.println("height: " + height);
    System.out.println("width2: " + width2);
    System.out.println("height2: " + height2);
    System.out.println();
    System.out.println("Matrix A:");
    A = increasingMatrix(width,height,true);//print A
    printMatrix(A,true);
    System.out.println();
    System.out.println();
    System.out.println("Matrix B:");
    B = increasingMatrix(width,height,false);//print B
    printMatrix(B,true);
    System.out.println();
    System.out.println();
    System.out.println("Matrix C:");
    C = increasingMatrix(width2,height2,true);//print C
    printMatrix(C,true);
    System.out.println();
    System.out.println();
    System.out.println("Matrix A + B:");//add A and B
    printMatrix(addMatrix(A,true,B,false),true);
    System.out.println();
    System.out.println();
    System.out.println("Matrix A + C:");//add A and C
    printMatrix(addMatrix(A,true,C,true),true);
  }
}