import java.util.Scanner; //import scanner
public class TwistGenerator{ //create class Twist Generator 
  public static void main(String [] args){ //Create Main Method
    Scanner getNumber = new Scanner(System.in); //Create Scanner
    int length = 0; //Initialize Length
    String junkWord = "hi"; //Initialize junkWord
    boolean check = false; //Initialize check
    
    while (check == false){ //create while loop for check
      System.out.println("Input a positive integer: "); //ask for positive integer
      if (getNumber.hasNextInt() == true){ //if it is an int then get length
        length = getNumber.nextInt(); //value for length
        if (length > 0){
          check = true; //check if it is positive
        }
        else{
          System.out.println("Enter a positive integer"); //check if it is positive
          check = false;//go back through loop 
        }
      }
      else {
        junkWord = getNumber.next();//junkword
        System.out.println("Please type in an integer");//redo loop 
      }
    }
    
    for (int i = 0; i <= length; i++){ //print with for loop slashes
      if (i%3 == 0){//if divisible by 3 then print \
        System.out.print("\\");
      }
      else if (i%2 == 0){ // if divisible by 2 print /
        System.out.print(" /");
      } 
    }
    System.out.println(""); //new line
    for (int i = 0; i <= length; i++){ //print X's
      if (i%3 == 0){ //if divisible by 3 print space
        System.out.print(" ");
      }
      else if (i%2 == 0){//if divisible by 2  print X
        System.out.print("X ");
      } 
    }
    System.out.println(""); //new line
    for (int i = 0; i <= length; i++){
      if (i%3 == 0){//if divisible by 3 print /
        System.out.print("/ ");
      }
      else if (i%2 == 0){ //if divisible by 2 print \
        System.out.print("\\");
      } 
    }
     
  }
}