import java.util.Scanner; //import Scanner
public class Convert { //create class Convert
  public static void main(String [] args){ //create main method
    Scanner newScanner = new Scanner(System.in); //Create Scanner
    System.out.println("Enter the affected area in acres: "); //ask for area in acres
    double acresArea = newScanner.nextDouble(); //User Input AcresArea
    System.out.println("Enter the rainfall in the affected area: "); //ask for inches of rainfall
    int inchesOfRain = newScanner.nextInt(); //User Input Inches Of Rain
    double acresToMiles = 0.0015625; //Convert acres to miles
    double inchesToMiles = 0.000015783; //Convert inches to miles
    double cubicMiles = (acresArea * acresToMiles)*(inchesOfRain * inchesToMiles); //Calculate Cubic Miles
    System.out.println(cubicMiles + " cubic miles"); //Print Cubic Miles
  }//end main method
}//end class