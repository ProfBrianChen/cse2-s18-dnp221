import java.util.Scanner; //import Scanner
public class Pyramid {//create class Pyramid
  public static void main(String [] args){//Create main Method
    Scanner newScanner = new Scanner(System.in); //Create Scanner
    System.out.println("The square side of the pyramid is (input length): "); //ask for length
    double length = newScanner.nextDouble(); //User Input length
    System.out.println("The height of the pyramid is (input height): "); //ask for height
    double height = newScanner.nextDouble(); //User Input height
    double volume = (length * length * height / 3); //Calculate volume
    System.out.println("The volume inside the pyramid is: " + volume); //Print Volume
  }//end main method
}//end class