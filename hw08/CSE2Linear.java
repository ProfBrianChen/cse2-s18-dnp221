import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
    
    static void shuffle(int[] array){
    Random rng = new Random();
      System.out.println("Random Scramble: ");
    for (int i = array.length - 1; i > 0; i--)
    {
      int index = rng.nextInt(i + 1);
      // Simple swap
      int a = array[index];
      array[index] = array[i];
      array[i] = a;
    }
  }
    
  public static void main(String[] args){
    Scanner scan = new Scanner(System.in);
    int[] studentGrades;
    studentGrades = new int[15];
    int grade; 
    System.out.println("Enter student grades in ascending order.");
    for (int i = 0 ; i < studentGrades.length; i++ ) {
        System.out.print("Enter student " + (i+1) + " Midterm grade: ");
           studentGrades[i] = scan.nextInt();
            if(i > 0){
              if(studentGrades[i-1] > studentGrades[i]){
                System.out.println("You did not put in ascending order.");
                System.exit(0);
              }
            }
            if (studentGrades[i] != (int) studentGrades[i]){
              System.out.println("You did not enter an integer.");

              System.exit(0);
            }
            
            if(studentGrades[i] > 100 || studentGrades[i] < 0){
              System.out.println("Grades must be between 0 and 100");
         
              System.exit(0);
            }
            
        }
    
     for (int i = 0 ; i < studentGrades.length; i++) {
           System.out.println(studentGrades[i]);
        }
    System.out.println("Enter a grade");
    grade = scan.nextInt();
    
    for(int i = 0; i < studentGrades.length; i++){
      if (grade == studentGrades[i]){
        System.out.println("Grade: "+ grade + " Iteration: " + i);
        break;
      }
      else if(i == studentGrades.length - 1 && grade != studentGrades[14]){
        System.out.println("Grade was not found");
      }
    }
    
    shuffle(studentGrades);
     for (int i = 0 ; i < studentGrades.length; i++) {
           System.out.print(studentGrades[i] + " ");
        }
    System.out.println();
    System.out.println("Enter a grade");
    grade = scan.nextInt();
    for(int i = 0; i < studentGrades.length; i++){
      if (grade == studentGrades[i]){
        System.out.println("Grade: "+ grade + " Iteration: " + i);
        break;
      }
      else if(i == studentGrades.length - 1 && grade != studentGrades[14]){
        System.out.println("Grade was not found");
      }
    }
  }
}