import java.util.Random; //import Random 
public class DrawPoker{//create class
  public static boolean fullHouse(int[] hand){//create fullHouse method
    int amount = 0;//set beginning amount
    int threeCard = 100;//set threeCard default
    boolean threeCardCheck = false;//create threeCardCheck
    int[] check = new int[5];//create check
    for(int i = 0; i<5; i++){//create for loop until 5
      check[i] = hand[i];//set check value from hand
      for(int j = i+1; j<5; j++){//for loop
        if(check[i]%13 == hand[j]%13){//check if hand value is check value
          amount++;//increase amount
        }   
        if(amount == 3){//if amount is 3 then threeCardCheck is true
          threeCard = hand[j]%13;
          threeCardCheck = true;
          break;
        }
      }
    }
    
    boolean pairCardCheck = false;//set pairCardCheck
    for(int i = 0; i<5; i++){//loop until end of hand
      check[i] = hand[i];//create value to check
      for(int j = i+1; j<5; j++){//check for a new 2 of kind
        if((check[i]%13 == hand[j]%13) && check[i]%13 != threeCard){
          pairCardCheck = true;
        }   
      }
    }
    if(pairCardCheck && threeCardCheck){//if both true return true 
      return true;
    }
    return false;//else false
  }
  
  public static boolean flush(int [] hand){//create flush method
    for(int i = 1; i<5; i++) {
      if (hand[0] / 13 != hand[i] / 13) {//compare hand to first card
        return false;//return false if not
      }
    }
    return true;
  }
  public static boolean threeOfAKind(int[] hand){//create 3 of kind
    int amount = 0;//create amount
    int[] check = new int[5];//create check
    for(int i = 0; i<5; i++){//loop until hand
      check[i] = hand[i];//check first card
      for(int j = i+1; j<5; j++){//loop until hand ends
        if(check[i]%13 == hand[j]%13){//if card# is check
          amount++;//increase amount
        }   
        if(amount == 3){//if amount is 3 return true
          return true;
        }
      }
    }
    return false;//else return false
  }
  
  public static void main(String[] args){//create main method
    int[] firstPlayerHand = new int[5];//create firstPlayerHand
    int[] secondPlayerHand = new int[5];//create secondPlayerHand
    int[] deck = new int[51];//create deck
    int firstScore = 0;//create scores
    int secondScore = 0;
    
    for(int i = 0; i < 51; i++){//create unshuffled deck
      deck[i] = i;
    }
    shuffleDeck(deck);//shuffleDeck
    System.out.println();
    System.out.println("Player 1 hand");//give firstPlayerHand and secondPlayerHand values
    
    int increase = 0;//set increase
    for(int u = 0; u<10; u++){//put values into hands
      if(u%2 == 0){//if even firstPlayerHand is deck[u]
        firstPlayerHand[increase] = deck[u];
      }
      if(u%2 == 1){
        secondPlayerHand[increase] = deck[u];//if odd secondPlayerHand is deck[u]
        increase++;//increase
      }
    }
    for(int i = 0; i < 5; i++){//print firstPlayerHand
      System.out.print(firstPlayerHand[i]%13 + " ");
    }
    System.out.println();
    System.out.println();
    System.out.println("Player 2 hand");//print secondPlayerHand
    for(int i = 0; i < 5; i++){System.out.print(secondPlayerHand[i]%13 + " ");}
    System.out.println();
    System.out.println();
    
    System.out.println("Pairs:");//print pairs
    boolean pairFirst = pair(firstPlayerHand);
    System.out.println("First hand Pair: " + pairFirst);
    if(pairFirst == true){firstScore++;}
    boolean pairSecond = pair(secondPlayerHand);
    System.out.println("First hand Pair: " +pairSecond);
    if(pairSecond == true){secondScore++;}
    
    System.out.println();
    System.out.println("Three of a kind:");//print 3 of a kind
    boolean threeOfAKindFirst = threeOfAKind(firstPlayerHand);
    System.out.println("First hand three of a kind: " +threeOfAKindFirst);
    if(threeOfAKindFirst == true){firstScore++;}
    boolean threeOfAKindSecond = threeOfAKind(secondPlayerHand);
    System.out.println("Second hand three of a kind: " +threeOfAKindSecond);
    if(threeOfAKindSecond == true){secondScore++;}
    
    System.out.println();
    System.out.println("Flush:");//print flush
    boolean flushFirst = flush(firstPlayerHand);
    System.out.println("First hand flush: " + flushFirst);
    if(flushFirst == true){firstScore++;}
    boolean flushSecond = flush(secondPlayerHand);
    System.out.println("Second hand flush: " + flushSecond);
    if(flushSecond == true){secondScore++;}
    
    System.out.println();
    System.out.println("Full House: ");//print fullHouse
    boolean fullHouseFirst = fullHouse(firstPlayerHand);
    if(fullHouseFirst == true){firstScore++;}
    System.out.println("First hand full house: " + fullHouseFirst);
    boolean fullHouseSecond = fullHouse(secondPlayerHand);
    System.out.println("Second hand full house: "+fullHouseSecond);
    
    if(fullHouseSecond == true){secondScore++;}
    if(firstScore > secondScore){System.out.println("Player 1 wins!");}
    else if(secondScore > firstScore){System.out.println("Player 2 wins!");}
    else{System.out.println("Tie");}
  }
  public static void shuffleDeck(int[] deck){//create shuffleDeck
    Random rand = new Random();//random
    for(int i = 50; i >= 0; i--){//shuffle deck randomly
      int pointer = rand.nextInt(i + 1);
      int array = deck[pointer];
      deck[pointer] = deck[i];
      deck[i] = array;
    }
  }
  public static boolean pair(int[] hand){//create pair method
    int[] check = new int[5];//create check
    for(int i = 0; i<5; i++){//for loop until hand ends
      check[i] = hand[i];//set check to card
      for(int j = i+1; j<5; j++){//check cards
        if(check[i]%13 == hand[j]%13){//return true if card is hand
          return true;
        }   
      }
    }
    return false;//else return false
  }
}