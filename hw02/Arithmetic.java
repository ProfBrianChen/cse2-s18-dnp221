public class Arithmetic{
  public static void main(String[]args){
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per box of envelopes
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    //total cost each item
    double totalCostOfPants = pantsPrice * numPants;
    double totalCostOfShirts = shirtPrice * numShirts;
    double totalCostOfBelts = beltCost * numBelts;
    
    //sales tax on each item
    double salesTaxOnPants = totalCostOfPants * paSalesTax;
    double salesTaxOnShirts = totalCostOfShirts * paSalesTax;
    double salesTaxOnBelts = totalCostOfBelts * paSalesTax;
    
    //total cost before tax
    double totalCost = totalCostOfBelts + totalCostOfShirts + totalCostOfPants;
    
    //total sales tax
    double totalSalesTaxCost = salesTaxOnBelts + salesTaxOnShirts + salesTaxOnPants;
    
    //total cost including tax
    double totalCostWithTax = totalSalesTaxCost + totalCost;
    
    //print out everything
    System.out.printf("Total cost of Pants: $%.2f\n", totalCostOfPants);
    System.out.printf("Total cost of Shirts: $%.2f\n", totalCostOfShirts);
    System.out.printf("Total cost of Belts: $%.2f\n", totalCostOfBelts);
    System.out.printf("Total sales tax on Pants: $%.2f\n", salesTaxOnPants);
    System.out.printf("Total sales tax on Shirts: $%.2f\n", salesTaxOnShirts);
    System.out.printf("Total sales tax on Belts: $%.2f\n", salesTaxOnBelts);
    System.out.printf("Total Cost before Tax: $%.2f\n", totalCost);
    System.out.printf("Total Sales Tax: $%.2f\n", totalSalesTaxCost);
    System.out.printf("Total Cost: $%.2f\n", totalCostWithTax);
    
    }
}