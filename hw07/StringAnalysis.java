import java.util.Scanner;

public class StringAnalysis{
  
  public static boolean fullString(String allStrings){
    int length = allStrings.length();
    for(int i = 0; i < length; i++){
      char letter = allStrings.charAt(i);
      if( letter < 'a' || letter > 'z'){
        return false;
      }
    }
    return true;
  }
  
  public static boolean partlyString(String someString, int someInt){
    int length = someString.length();
    for(int i = 0; i < someInt; i++){
      char letter = someString.charAt(i);
      if (i == length){
        i = someInt;
      }
      if( letter < 'a' || letter > 'z'){
        return false;
      }
    }
    return true;
  }
    
  public static void main(String args[]){
    Scanner scan = new Scanner(System.in);
    System.out.println("Enter a String");
    String userString = scan.next();
    System.out.println("Entire string (1) or just part of a string (2)? 1 or 2");
    int decison = scan.nextInt();
    if (decison == 1){
      if (fullString(userString) == true){
        System.out.println("there are only letters in the string");
      }
      else
        System.out.println("there are non-letters in the string");
    }
    else if(decison == 2){
      System.out.println("How many characters should be checked?");
      int amount = scan.nextInt();
      if (partlyString(userString, amount) == true){
        System.out.println("there are only letters in the string");
      }
      else 
        System.out.println("there are non-letters in the string");
    }
    else{
      System.out.println("You did not enter a 1 or 2");
    }
  }
}