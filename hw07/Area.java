import java.util.Scanner;//import scanner

public class Area{//create class Area
  public static void main(String [] args){//main method
    Scanner scan = new Scanner(System.in);//create scanner
    System.out.println("What shape would you like to find the area for?");//ask what shape
    String shape = scan.next();//get shape
    checkShape(shape);//run check the shape
  }
  public static double rectangleArea(double length, double width){ //create rectangleArea
    double areaOfRect;//declare areaOfRect
    areaOfRect = length * width;//create areaOfRect
    System.out.println(areaOfRect);//print areaOfRect
    return(areaOfRect);//return areaOfRect
  }
  public static double triangleArea(double length, double height){//create triangleArea
    double areaOfTri;//declare areaOfTri
    areaOfTri = .5 * length * height;//create areaOfTri
    System.out.println(areaOfTri);//print areaOfTri
    return areaOfTri;//return areaOfTri
  }
  public static double circleArea(double radius){//create circleArea
    double areaOfCirc;//declare areaOfCirc
    areaOfCirc = Math.PI * radius * radius;//create areaOfCirc
    System.out.println(areaOfCirc);//print areaOfCirc
    return(areaOfCirc);//return areaOfCirc
  }
  public static void checkShape(String shape){//create checkShape
     Scanner scan = new Scanner(System.in);//scanner scan
    if (shape.equals("triangle")){//check if shape is a triangle
      System.out.println("intput length in double");//user input length
      double length = scan.nextDouble();//get length
      System.out.println("input height in double");//user input height
      double height = scan.nextDouble();//get height
      triangleArea(length, height);//run triangleArea
      return;//return
    }
    if (shape.equals("rectangle")){//if shape is rectangle
      System.out.println("intput length in double");//user input length
      double length = scan.nextDouble();//get length
      System.out.println("input width in double");//user input width
      double width = scan.nextDouble();//get width
      rectangleArea(length, width);//run rectangleArea
      return;//return
    } 
    if (shape.equals("circle")){//if shape is circle
      System.out.println("intput radius in double");//user input radius
      double radius = scan.nextDouble();//get radius
      circleArea(radius);//run circleArea
      return;//return
    }
    else{//user didn't print triangle, rectangle, or circle
      System.out.println("You did not enter triangle, circle, or rectangle.");//print
      return;//return
    }
  }
}