import java.util.Scanner; //import user scanner
import java.util.Random;//import random scanner

public class array{ //class array
  public static void main(String[] args){ //main method
    Random number = new Random(); //declare random 
    Scanner scan = new Scanner(System.in); //create scanner
    String[] name; //create String name
    name = new String[5]; //create name
    int[] grade; //create in grades
    grade = new int[5]; //create grade
    
    for(int i = 0; i < 5; i++){ //for loop
      System.out.println("Input name "+ (i+1)); //input name
      name[i] = scan.nextLine(); //get input
      grade[i] = number.nextInt(101); //declare grade
    }
    System.out.println("Student Grades and Midterm"); //print 
    for (int i = 0; i < 5; i++){
      System.out.println(name[i] + ": " + grade[i]);
    }
  }
}